﻿using Xunit;
using RpgCharacters.Weapons;
using RpgCharacters.Shared;

namespace UnitTests
{
    public class TestWeapons
    {
        [Theory]
        [InlineData(WeaponTypes.MELEE, 17)]
        [InlineData(WeaponTypes.RANGED, 8)]
        [InlineData(WeaponTypes.MAGIC, 27)]
        public void Create_level_1_weapon__Should_have_correct_damage(WeaponTypes weaponType, int expected)
        {
            int actual = new Weapon(weaponType, 1).GetDamage(new Stats());

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(WeaponTypes.MELEE, 25)]
        [InlineData(WeaponTypes.RANGED, 20)]
        [InlineData(WeaponTypes.MAGIC, 35)]
        public void Create_level_5_weapon__Should_have_correct_damage(WeaponTypes weaponType, int expected)
        {
            int actual = new Weapon(weaponType, 5).GetDamage(new Stats());

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(WeaponTypes.MELEE, 32)]
        [InlineData(WeaponTypes.RANGED, 8)]
        [InlineData(WeaponTypes.MAGIC, 27)]
        public void Level_1_weapon__Wielder_has_10_strength__Should_have_correct_damage(WeaponTypes weaponType, int expected)
        {
            int actual = new Weapon(weaponType, 1).GetDamage(new Stats(0, 10, 0, 0));

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(WeaponTypes.MELEE, 17)]
        [InlineData(WeaponTypes.RANGED, 28)]
        [InlineData(WeaponTypes.MAGIC, 27)]
        public void Level_1_weapon__Wielder_has_10_dexterity__Should_have_correct_damage(WeaponTypes weaponType, int expected)
        {
            int actual = new Weapon(weaponType, 1).GetDamage(new Stats(0, 0, 10, 0));

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(WeaponTypes.MELEE, 17)]
        [InlineData(WeaponTypes.RANGED, 8)]
        [InlineData(WeaponTypes.MAGIC, 57)]
        public void Level_1_weapon__Wielder_has_10_intelligence__Should_have_correct_damage(WeaponTypes weaponType, int expected)
        {
            int actual = new Weapon(weaponType, 1).GetDamage(new Stats(0, 0, 0, 10));

            Assert.Equal(expected, actual);
        }
    }
}
