﻿using Xunit;
using RpgCharacters.Shared;
using RpgCharacters.Armor;

namespace UnitTests
{
    public class TestArmor
    {
        #region Testing Armor Pieces

        [Fact]
        public void Create_armor_piece__With_no_arguments__Should_have_stats_bonuses_of_0()
        {
            int expected = 0;

            Stats actual = new ArmorPiece().StatsBonus;

            Assert.Equal(expected, actual.Health);
            Assert.Equal(expected, actual.Strength);
            Assert.Equal(expected, actual.Dexterity);
            Assert.Equal(expected, actual.Intelligence);
        }

        [Theory]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.BODY, new int[] { 15, 0, 2, 5 })]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.HEAD, new int[] { 12, 0, 1, 4 })]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.LEGS, new int[] { 9, 0, 1, 3 })]

        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.BODY, new int[] { 28, 2, 5, 0 })]
        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.HEAD, new int[] { 22, 1, 4, 0 })]
        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.LEGS, new int[] { 16, 1, 3, 0 })]

        [InlineData(ArmorMaterials.PLATE, ArmorSlots.BODY, new int[] { 42, 5, 2, 0 })]
        [InlineData(ArmorMaterials.PLATE, ArmorSlots.HEAD, new int[] { 33, 4, 1, 0 })]
        [InlineData(ArmorMaterials.PLATE, ArmorSlots.LEGS, new int[] { 25, 3, 1, 0 })]
        public void Create_level_1_armor_piece__Should_have_correct_stats_bonus(ArmorMaterials material, ArmorSlots slot, int[] expected)
        {
            Stats actual = new ArmorPiece(material, slot, 1).StatsBonus;

            Assert.Equal(expected[0], actual.Health);
            Assert.Equal(expected[1], actual.Strength);
            Assert.Equal(expected[2], actual.Dexterity);
            Assert.Equal(expected[3], actual.Intelligence);
        }

        [Theory]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.BODY, new int[] { 35, 0, 6, 13 })]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.HEAD, new int[] { 28, 0, 4, 10 })]
        [InlineData(ArmorMaterials.CLOTH, ArmorSlots.LEGS, new int[] { 21, 0, 3, 7 })]

        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.BODY, new int[] { 60, 6, 13, 0 })]
        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.HEAD, new int[] { 48, 4, 10, 0 })]
        [InlineData(ArmorMaterials.LEATHER, ArmorSlots.LEGS, new int[] { 36, 3, 7, 0 })]

        [InlineData(ArmorMaterials.PLATE, ArmorSlots.BODY, new int[] { 90, 13, 6, 0 })]
        [InlineData(ArmorMaterials.PLATE, ArmorSlots.HEAD, new int[] { 72, 10, 4, 0 })]
        [InlineData(ArmorMaterials.PLATE, ArmorSlots.LEGS, new int[] { 54, 7, 3, 0 })]
        public void Create_level_5_armor_piece__Should_have_correct_stats_bonus(ArmorMaterials material, ArmorSlots slot, int[] expected)
        {
            Stats actual = new ArmorPiece(material, slot, 5).StatsBonus;

            Assert.Equal(expected[0], actual.Health);
            Assert.Equal(expected[1], actual.Strength);
            Assert.Equal(expected[2], actual.Dexterity);
            Assert.Equal(expected[3], actual.Intelligence);
        }

        #endregion

        #region Testing Armer Sets

        [Fact]
        public void Create_armor_set__With_no_arguments__Should_have_stats_bonuses_of_0()
        {
            int expected = 0;

            Stats actual = new ArmorSet().StatsBonus;

            Assert.Equal(expected, actual.Health);
            Assert.Equal(expected, actual.Strength);
            Assert.Equal(expected, actual.Dexterity);
            Assert.Equal(expected, actual.Intelligence);
        }


        // There should be tests on equiping armor pieses into an armor set, but they seem more like integration tests,
        // or mocking is needed, so I'll leave them for now.

        // There should also be tests on the armor factory.

        #endregion
    }
}
