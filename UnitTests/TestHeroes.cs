using Xunit;
using RpgCharacters.Heroes;

namespace UnitTests
{
    public class TestHeroes
    {

        #region Testing Hero Creation

        [Theory]
        [InlineData(HeroClass.WARRIOR)]
        [InlineData(HeroClass.RANGER)]
        [InlineData(HeroClass.MAGE)]
        public void Create_new_hero__Should_be_level_1(HeroClass heroClass)
        {
            int expected = 1;

            int actual = HeroFactory.CreateHero(heroClass).Level;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 150)]
        [InlineData(HeroClass.RANGER, 120)]
        [InlineData(HeroClass.MAGE, 100)]
        public void Create_new_hero__Should_have_correct_health(HeroClass heroClass, int expected)
        {
            int actual = HeroFactory.CreateHero(heroClass).Health;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 10)]
        [InlineData(HeroClass.RANGER, 5)]
        [InlineData(HeroClass.MAGE, 2)]
        public void Create_new_hero__Should_have_correct_strength(HeroClass heroClass, int expected)
        {
            int actual = HeroFactory.CreateHero(heroClass).Strength;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 3)]
        [InlineData(HeroClass.RANGER, 10)]
        [InlineData(HeroClass.MAGE, 3)]
        public void Create_new_hero__Should_have_correct_dexterity(HeroClass heroClass, int expected)
        {
            int actual = HeroFactory.CreateHero(heroClass).Dexterity;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 1)]
        [InlineData(HeroClass.RANGER, 2)]
        [InlineData(HeroClass.MAGE, 10)]
        public void Create_new_hero__Should_have_correct_intelligence(HeroClass heroClass, int expected)
        {
            int actual = HeroFactory.CreateHero(heroClass).Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Testing Gaining Experience

        [Fact]
        public void Gaining_xp__Under_level_threshold__Hero_should_be_same_level()
        {
            int source = 99;
            int expected = 1;
            Hero hero = HeroFactory.CreateHero(HeroClass.WARRIOR);

            hero.GainExperience(source);
            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(110, 2)]
        [InlineData(220, 3)]
        [InlineData(500, 5)]
        public void Gaining_xp__Over_level_threshold__Hero_should_be_correct_level(int source, int expected)
        {
            Hero hero = HeroFactory.CreateHero(HeroClass.WARRIOR);

            hero.GainExperience(source);
            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(100, 2)]
        [InlineData(210, 3)]
        [InlineData(464, 5)]
        [InlineData(770, 7)]
        public void Gaining_xp__Exactly_on_level_threshold__Hero_should_be_correct_level(int source, int expected)
        {
            Hero hero = HeroFactory.CreateHero(HeroClass.WARRIOR);

            hero.GainExperience(source);
            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Gain_xp__Half_level_threshold_twice__Hero_should_level_up()
        {
            int source = 50;
            int expected = 2;
            Hero hero = HeroFactory.CreateHero(HeroClass.WARRIOR);

            hero.GainExperience(source);
            hero.GainExperience(source);
            int actual = hero.Level;

            Assert.Equal(expected, actual);
        }

        #endregion

        #region Testing Leveling Up

        [Theory]
        [InlineData(HeroClass.WARRIOR, 180)]
        [InlineData(HeroClass.RANGER, 140)]
        [InlineData(HeroClass.MAGE, 115)]
        public void Level_up_hero__From_level_1__Hero_should_have_correct_health(HeroClass heroClass, int expected)
        {
            Hero hero = HeroFactory.CreateHero(heroClass);

            hero.GainExperience(100);
            int actual = hero.Health;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 15)]
        [InlineData(HeroClass.RANGER, 7)]
        [InlineData(HeroClass.MAGE, 3)]
        public void Level_up_hero__From_level_1__Hero_should_have_correct_strength(HeroClass heroClass, int expected)
        {
            Hero hero = HeroFactory.CreateHero(heroClass);

            hero.GainExperience(100);
            int actual = hero.Strength;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 5)]
        [InlineData(HeroClass.RANGER, 15)]
        [InlineData(HeroClass.MAGE, 5)]
        public void Level_up_hero__From_level_1__Hero_should_have_correct_dexterity(HeroClass heroClass, int expected)
        {
            Hero hero = HeroFactory.CreateHero(heroClass);

            hero.GainExperience(100);
            int actual = hero.Dexterity;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(HeroClass.WARRIOR, 2)]
        [InlineData(HeroClass.RANGER, 3)]
        [InlineData(HeroClass.MAGE, 15)]
        public void Level_up_hero__From_level_1__Hero_should_have_correct_intelligence(HeroClass heroClass, int expected)
        {
            Hero hero = HeroFactory.CreateHero(heroClass);

            hero.GainExperience(100);
            int actual = hero.Intelligence;

            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
