﻿using RpgCharacters.Shared;

namespace RpgCharacters.Armor
{
    // Defines a single piece of armor.
    public class ArmorPiece
    {
        private readonly ArmorMaterials material;
        private readonly int level;


        // Public properties.
        public readonly ArmorSlots Slot;
        public Stats StatsBonus { get; private set; }

        // Constructor that sets material, slot and level, and calculates bonus stats.
        public ArmorPiece(ArmorMaterials material, ArmorSlots slot, int level)
        {
            Slot = slot;
            this.material = material;
            this.level = level;

            CalculateStatsBonus();
        }

        // Empty constructor for unarmored.
        public ArmorPiece()
        {
            StatsBonus = new Stats();
        }

        // Calculates the stat bonuses this armor gives, based on material and slot.
        private void CalculateStatsBonus()
        {
            int healthBonus = 0;
            int strengthBonus = 0;
            int dexterityBonus = 0;
            int intelligenceBonus = 0;

            // Calculate the base bonus based on material.
            switch (material)
            {
                case ArmorMaterials.CLOTH:
                    healthBonus = 10 + 5 * level;
                    intelligenceBonus = 3 + 2 * level;
                    dexterityBonus = 1 + 1 * level;
                    break;
                case ArmorMaterials.LEATHER:
                    healthBonus = 20 + 8 * level;
                    dexterityBonus = 3 + 2 * level;
                    strengthBonus = 1 + 1 * level;
                    break;
                case ArmorMaterials.PLATE:
                    healthBonus = 30 + 12 * level;
                    strengthBonus = 3 + 2 * level;
                    dexterityBonus = 1 + 1 * level;
                    break;
                default:
                    break;
            }

            // Calculate scaling factor based on armor slot.
            double scalingFactor = Slot switch
            {
                ArmorSlots.BODY => 1.0,
                ArmorSlots.HEAD => 0.8,
                ArmorSlots.LEGS => 0.6,
                _ => 0,
            };

            // Set the public property to the calculated values.
            StatsBonus = new Stats(
                (int)(healthBonus * scalingFactor),
                (int)(strengthBonus * scalingFactor),
                (int)(dexterityBonus * scalingFactor),
                (int)(intelligenceBonus * scalingFactor)
                );
        }

        // Return a string representation of the armor piece.
        public override string ToString()
        {
            return $"Armor stats:\n" +
                $"Type: {material}\n" +
                $"Slot: {Slot}\n" +
                $"Level: {level}\n" +
                $"{StatsBonus.ToString("Bonus")}";
        }
    }
}
