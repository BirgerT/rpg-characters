﻿using System;

namespace RpgCharacters.Armor
{
    public static class ArmorFactory
    {
        // Creates an armor piece with no stats bonuses. (unarmored)
        public static ArmorPiece CreateArmorPiece()
        {
            return new ArmorPiece();
        }

        // Creates an armor piece.
        public static ArmorPiece CreateArmorPiece(ArmorMaterials material, ArmorSlots slot, int level)
        {
            return new ArmorPiece(material, slot, level);
        }


        // Creates an unarmored armor set.
        public static ArmorSet CreateArmorSet()
        {
            return new ArmorSet();
        }

        // Creates a full set of armor of the given material and level.
        public static ArmorSet CreateArmorSet(ArmorMaterials material, int level)
        {
            // Create an empty armor set.
            ArmorSet armorSet = new ArmorSet();

            // Add an armor piece to earch slot.
            foreach (ArmorSlots slot in slots)
            {
                armorSet.Equip(new ArmorPiece(material, slot, level));
            }

            return armorSet;
        }

        // Create a random set of armor.
        public static ArmorSet CreateRandomArmorSet()
        {
            Random random = new Random();

            // Create an empty armor set.
            ArmorSet armorSet = new ArmorSet();

            // Add random armor piece to each slot.
            foreach (ArmorSlots slot in slots)
            {
                ArmorMaterials material = (ArmorMaterials)materials.GetValue(random.Next(materials.Length));
                int level = random.Next(100);
                armorSet.Equip(new ArmorPiece(material, slot, level));
            }

            return armorSet;
        }

        // Get all the available slots.
        private static readonly Array slots = Enum.GetValues(typeof(ArmorSlots));

        // Get all the available materials.
        private static readonly Array materials = Enum.GetValues(typeof(ArmorMaterials));
    }
}
