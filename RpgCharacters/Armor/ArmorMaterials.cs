﻿namespace RpgCharacters.Armor
{
    public enum ArmorMaterials
    {
        CLOTH,
        LEATHER,
        PLATE,
    }
}
