﻿using RpgCharacters.Shared;

namespace RpgCharacters.Armor
{
    // Defines a full set of armor. Can consist of any combination of armor pieces.
    public class ArmorSet
    {
        // Initialize all armor slots as unarmored.
        private ArmorPiece headArmor = ArmorFactory.CreateArmorPiece();
        private ArmorPiece bodyArmor = ArmorFactory.CreateArmorPiece();
        private ArmorPiece legsArmor = ArmorFactory.CreateArmorPiece();

        // Total stats of all the armor pieces;
        public Stats StatsBonus
        {
            get => headArmor.StatsBonus + bodyArmor.StatsBonus + legsArmor.StatsBonus;
        }

        // Equips the armor to its correct place, overriding anything that was there before.
        public void Equip(ArmorPiece armor)
        {
            switch (armor.Slot)
            {
                case ArmorSlots.HEAD:
                    headArmor = armor;
                    break;
                case ArmorSlots.BODY:
                    bodyArmor = armor;
                    break;
                case ArmorSlots.LEGS:
                    legsArmor = armor;
                    break;
                default:
                    break;
            }
        }

        // Equpis a full set of armor, overriding any equiped armor.
        public void Equip(ArmorSet armor)
        {
            headArmor = armor.headArmor;
            bodyArmor = armor.bodyArmor;
            legsArmor = armor.legsArmor;
        }
    }
}
