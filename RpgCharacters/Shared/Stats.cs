﻿using System.Text;

namespace RpgCharacters.Shared
{
    // Defines a set of stats, that can be added together to form even greater stats.
    public struct Stats
    {
        // Here are the stats.
        public int Health;
        public int Strength;
        public int Dexterity;
        public int Intelligence;

        public Stats(int health, int strength, int dexterity, int intelligence)
        {
            Health = health;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        // Allows for adding two sets of stats together.
        public static Stats operator +(Stats a, Stats b)
        {
            return new Stats
            (
                a.Health + b.Health,
                a.Strength + b.Strength,
                a.Dexterity + b.Dexterity,
                a.Intelligence + b.Intelligence
            );
        }

        // Returns a string representation of the stast.
        public override string ToString()
        {
            return ToString("");
        }

        // Returns a string representation of the stats, with a prefix in front of each stat.
        public string ToString(string prefix)
        {
            StringBuilder stats = new StringBuilder();

            // If there is a prefix, add a space behind it.
            if (prefix != "") prefix += " ";

            // Add each stat that is not 0 to the builder.
            if (Health != 0) stats.AppendLine($"{prefix}HP: {Health}");
            if (Strength != 0) stats.AppendLine($"{prefix}Str: {Strength}");
            if (Dexterity != 0) stats.AppendLine($"{prefix}Dex: {Dexterity}");
            if (Intelligence != 0) stats.AppendLine($"{prefix}Int: {Intelligence}");

            return stats.ToString();
        }
    }
}
