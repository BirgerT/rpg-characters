﻿using System;
using RpgCharacters.Heroes;
using RpgCharacters.Weapons;
using RpgCharacters.Armor;

namespace RpgCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Showcase assignment text examples

            // Create a level 9 warrior.
            Hero warrior = HeroFactory.CreateHero(HeroClass.WARRIOR);
            warrior.GainExperience(1200);
            Console.WriteLine(warrior + "\n");

            // Create a level 12 ranger.
            Hero ranger = HeroFactory.CreateHero(HeroClass.RANGER);
            ranger.GainExperience(2000);
            Console.WriteLine(ranger + "\n");

            // Create a level 5 melee weapon.
            Weapon axe = WeaponFactory.CreateWeapon(WeaponTypes.MELEE, 5);
            Console.WriteLine(axe + "\n");

            // Create a level 10 ranged weapon.
            Weapon bow = WeaponFactory.CreateWeapon(WeaponTypes.RANGED, 10);
            Console.WriteLine(bow + "\n");

            // Create level 10 cloth leg armor.
            ArmorPiece leggings = ArmorFactory.CreateArmorPiece(ArmorMaterials.CLOTH, ArmorSlots.LEGS, 10);
            Console.WriteLine(leggings);

            // Create level 15 plate body armor.
            ArmorPiece chestPlate = ArmorFactory.CreateArmorPiece(ArmorMaterials.PLATE, ArmorSlots.BODY, 15);
            Console.WriteLine(chestPlate);

            // Equip warrior with level 5 melee weapon and plate body armor.
            warrior.Equip(axe);
            warrior.Equip(ArmorFactory.CreateArmorPiece(ArmorMaterials.PLATE, ArmorSlots.BODY, 5));
            Console.WriteLine(warrior);

            warrior.Attack();

            #endregion

            #region The Adventure of Bob the Warrior.

            // Create a new warrior.
            Hero warriorBob = HeroFactory.CreateHero(HeroClass.WARRIOR);
            Console.WriteLine("\nBob the warrior was born. (Create warrior)");
            warriorBob.Attack();

            // Equip ranged weapon.
            warriorBob.Equip(WeaponFactory.CreateWeapon(WeaponTypes.RANGED, 1));
            Console.WriteLine("\nBob picked up a stick. (Equip level 1 ranged weapon)");
            warriorBob.Attack();

            // Level up.
            warriorBob.GainExperience(150);
            Console.WriteLine("\nBob did some push-ups. (Gain 150 xp)");
            warriorBob.Attack();

            // Equip armor.
            warriorBob.Equip(ArmorFactory.CreateArmorPiece(ArmorMaterials.CLOTH, ArmorSlots.BODY, 3));
            Console.WriteLine("\nBob put on a sweater. (Equip level 3 cloth body armor)");
            warriorBob.Attack();

            // Equip magic weapon.
            warriorBob.Equip(WeaponFactory.CreateWeapon(WeaponTypes.MAGIC, 2));
            Console.WriteLine("\nTurns out the stick was a magic wand. (Equip level 2 magic weapon)");
            warriorBob.Attack();

            // Level up using the rest from last xp gain.
            warriorBob.GainExperience(60);
            Console.WriteLine("\nBob did some pull-ups. (Gain 60 xp)");
            warriorBob.Attack();

            // Equip different armor.
            warriorBob.Equip(ArmorFactory.CreateArmorSet(ArmorMaterials.PLATE, 3));
            Console.WriteLine("\nBob put on his battle armor. (Equip full level 3 plate armor)");
            warriorBob.Attack();

            // Equip melee weapon.
            warriorBob.Equip(WeaponFactory.CreateWeapon(WeaponTypes.MELEE, 5));
            Console.WriteLine("\nBob found his battle axe. (Equip level 5 melee weapon)");
            warriorBob.Attack();

            // Gain a shit ton of XP.
            warriorBob.GainExperience(100000);
            Console.WriteLine("\nBob drank his morning coffee. (Gain 100000 xp)");
            warriorBob.Attack();

            // Equip insane hat.
            warriorBob.Equip(ArmorFactory.CreateArmorPiece(ArmorMaterials.PLATE, ArmorSlots.HEAD, 14000000));
            Console.WriteLine("\nBob found an insane hat. (Equip level 14 million plate head armor)");
            warriorBob.Attack();

            // Equip worse hat.
            warriorBob.Equip(ArmorFactory.CreateArmorPiece(ArmorMaterials.LEATHER, ArmorSlots.HEAD, 14));
            Console.WriteLine("\nThe hat ran out of power. (Equip level 14 leather head armor)");
            warriorBob.Attack();

            // Equip full set of armor.
            warriorBob.Equip(ArmorFactory.CreateArmorSet(ArmorMaterials.CLOTH, 1));
            Console.WriteLine("\nBob puts on his PJs. (Equip full set of level 1 cloth armor.)");
            warriorBob.Attack();

            // Fin.
            Console.WriteLine("\nBob goes to sleep after a long day.");

            #endregion

            #region Generate random hero

            Console.WriteLine(HeroFactory.CreateRandomHero());

            #endregion
        }
    }
}
