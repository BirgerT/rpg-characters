﻿using System;
using RpgCharacters.Shared;

namespace RpgCharacters.Heroes
{
    // Defines a warrior and inherits from hero.
    class Warrior : Hero
    {
        // Set the brutal stats of the hero.
        static Stats baseStats = new Stats(150, 10, 3, 1);
        static Stats levelUpIncrements = new Stats(30, 5, 2, 1);

        // Use the hero constructor with the brutal stats.
        public Warrior() : base(baseStats, levelUpIncrements) { }
        
        // Let the warrior believe that he attacked someone.
        public override void Attack()
        {
            if (equipedWeapon != null)
            {
                // If he has an axe, there will be damage displayed.
                Console.WriteLine($"Warrior attacks for {equipedWeapon.GetDamage(Stats)} damage");
            }
            else
            {
                // No axe, no damage!
                Console.WriteLine("Warrior has no weapon");
            }
        }

        // Display details about the brute.
        public override string ToString()
        {
            return ToString("Warrior");
        }
    }
}
