﻿using System;
using RpgCharacters.Weapons;
using RpgCharacters.Armor;

namespace RpgCharacters.Heroes
{
    public static class HeroFactory
    {
        // Using factory pattern to create new hero based on the given hero class.
        public static Hero CreateHero(HeroClass heroClass)
        {
            return heroClass switch
            {
                HeroClass.WARRIOR => new Warrior(),
                HeroClass.RANGER => new Ranger(),
                HeroClass.MAGE => new Mage(),
                _ => null,
            };
        }

        // Creates a random hero, fully decked out with with random gear.
        public static Hero CreateRandomHero()
        {
            Random random = new Random();

            // Generate random class.
            Array classes = Enum.GetValues(typeof(HeroClass));
            HeroClass randomClass = (HeroClass)classes.GetValue(random.Next(classes.Length));

            // Create a hero of the random class.
            Hero randomHero = CreateHero(randomClass);
            
            // Generate random equipment.
            Weapon randomWeapon = WeaponFactory.CreateRandomWeapon();
            ArmorSet randomArmor = ArmorFactory.CreateRandomArmorSet();

            // Equip the random gear.
            randomHero.Equip(randomWeapon);
            randomHero.Equip(randomArmor);

            // Give the hero a random amount of XP.
            randomHero.GainExperience(GetRequiredExperience(random.Next(99)));

            return randomHero;
        }


        // Get the required amount of XP to level up the given amount of times.
        private static int GetRequiredExperience(int levelUps)
        {
            int required = 100;
            int currentThreshold = 100;
            for (int i = 0; i < levelUps - 1; i++)
            {
                currentThreshold = (int)(currentThreshold * 1.1);
                required += currentThreshold;
            }

            return required;
        }
    }
}
