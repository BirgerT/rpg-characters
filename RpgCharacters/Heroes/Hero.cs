﻿using RpgCharacters.Shared;
using RpgCharacters.Weapons;
using RpgCharacters.Armor;

namespace RpgCharacters.Heroes
{
    // A base hero class that every hero class extends.
    public abstract class Hero
    {
        private Stats baseStats;
        private Stats levelUpIncrements;

        // Experience for leveling.
        private int experience;
        private int xpLevelThreshold;

        // Equipment.
        protected Weapon equipedWeapon;
        private readonly ArmorSet equipedArmor;

        // Stats including bonuses from armor.
        protected Stats Stats { get => baseStats + equipedArmor.StatsBonus; }


        // Public properties returning individual stats, including bonuses.
        public int Level { get; protected set; }
        public int Health { get { return Stats.Health; } }
        public int Strength { get { return Stats.Strength; } }
        public int Dexterity { get { return Stats.Dexterity; } }
        public int Intelligence { get { return Stats.Intelligence; } }


        // Protected constructor, so that a base Hero can not be created.
        // Called by the hero classes, with their respective stats and level up increments.
        protected Hero(Stats baseStats, Stats levelUpIncrements)
        {
            // Set fields of a level 1 hero.
            Level = 1;
            experience = 0;
            xpLevelThreshold = 100;

            // Set fields based on the hero class.
            this.baseStats = baseStats;
            this.levelUpIncrements = levelUpIncrements;

            // Equip an empty set of armor (unarmored).
            equipedArmor = ArmorFactory.CreateArmorSet();
        }

        // Increase the heros experience, and potensially level up.
        public void GainExperience(int xp)
        {
            experience += xp;

            // If enough experience was gained, level up the appropriate number of times.
            while (experience >= xpLevelThreshold)
            {
                LevelUp();
                experience -= xpLevelThreshold;
                // Increase the next level up threshold by 10%
                xpLevelThreshold = (int)(xpLevelThreshold * 1.1);
            }
        }

        // Levels up the character by increasing their stats according to the levelUpIncrements provided by hero classes.
        private void LevelUp()
        {
            Level++;
            baseStats.Health += levelUpIncrements.Health;
            baseStats.Strength += levelUpIncrements.Strength;
            baseStats.Dexterity += levelUpIncrements.Dexterity;
            baseStats.Intelligence += levelUpIncrements.Intelligence;
        }

        // Equip a weapon.
        public void Equip(Weapon weapon)
        {
            equipedWeapon = weapon;
        }

        // Equips a piece of armor.
        public void Equip(ArmorPiece armor)
        {
            equipedArmor.Equip(armor);
        }

        // Equip a full set of armor.
        public void Equip(ArmorSet armor)
        {
            equipedArmor.Equip(armor);
        }

        // Attack is implemented by the individual hero classes.
        public abstract void Attack();


        // Display details about the hero.
        protected string ToString(string className)
        {
            return $"{className} details:\n" +
                $"{Stats}" +
                $"Lvl: {Level}";
        }
    }
}
