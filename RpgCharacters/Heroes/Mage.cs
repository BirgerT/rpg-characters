﻿using System;
using RpgCharacters.Shared;

namespace RpgCharacters.Heroes
{
    // Defines a hero of the mage variety.
    class Mage : Hero
    {
        // Set stats to the default value for mages.
        static Stats baseStats = new Stats(100, 2, 3, 10);
        static Stats levelUpIncrements = new Stats(15, 1, 2, 5);

        // Calls the hero constructor with the mage values.
        public Mage() : base(baseStats, levelUpIncrements) { }

        // Displays a message about the attack. For testing purposes.
        public override void Attack()
        {
            if (equipedWeapon != null)
            {
                // If a weapon is equiped, the damage done is displayed.
                Console.WriteLine($"Mage attacks for {equipedWeapon.GetDamage(Stats)} damage");
            }
            else
            {
                // If no weapon is equiped, no damage can be done.
                Console.WriteLine("Mage has no weapon");
            }
        }

        // Display details about magic man.
        public override string ToString()
        {
            return ToString("Mage");
        }
    }
}
