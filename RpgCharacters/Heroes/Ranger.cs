﻿using System;
using RpgCharacters.Shared;

namespace RpgCharacters.Heroes
{
    // Defines a ranger in the image of a hero.
    class Ranger : Hero
    {
        // Set the stats of a basic ranger.
        static Stats baseStats = new Stats(120, 5, 10, 2);
        static Stats levelUpIncrements = new Stats(20, 2, 5, 1);

        // Call the hero constructor with mr rangers stats.
        public Ranger() : base(baseStats, levelUpIncrements) { }

        // Pretends to attack an enemy. For testing purposes
        public override void Attack()
        {
            if (equipedWeapon != null)
            {
                // If the ranger has a weapon, display the damage delt.
                Console.WriteLine($"Ranger attacks for {equipedWeapon.GetDamage(Stats)} damage");
            }
            else
            {
                // With no weapon equiped, no damage is delt.
                Console.WriteLine("Ranger has no weapon");
            }
        }

        // Display details about mr bowman.
        public override string ToString()
        {
            return ToString("Ranger");
        }
    }
}
