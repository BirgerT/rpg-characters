﻿namespace RpgCharacters.Weapons
{
    public enum WeaponTypes
    {
        MELEE,
        RANGED,
        MAGIC,
    }
}
