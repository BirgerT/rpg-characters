﻿using RpgCharacters.Shared;

namespace RpgCharacters.Weapons
{
    // Defines a weapon.
    public class Weapon
    {
        private readonly WeaponTypes weaponType;
        private readonly int level;

        // Create a new weapon based on type and level.
        public Weapon(WeaponTypes weaponType, int level)
        {
            this.weaponType = weaponType;
            this.level = level;
        }

        // Returns the base damage for the weapon.
        private int GetDamage()
        {
            return GetDamage(new Stats());
        }

        // Returns the total damage dealt, based on the weapon and the given stats.
        public int GetDamage(Stats characterStats)
        {
            return weaponType switch
            {
                WeaponTypes.MELEE => 15 + 2 * level + (int)(characterStats.Strength * 1.5),
                WeaponTypes.RANGED => 5 + 3 * level + (characterStats.Dexterity * 2),
                WeaponTypes.MAGIC => 25 + 2 * level + (characterStats.Intelligence * 3),
                _ => 0,
            };
        }

        // Return a string representation of the weapon.
        public override string ToString()
        {
            return $"Weapon stats:\n" +
                $"Type: {weaponType}\n" +
                $"Level: {level}\n" +
                $"Damage: {GetDamage()}";
        }
    }
}
