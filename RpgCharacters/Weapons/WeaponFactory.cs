﻿using System;

namespace RpgCharacters.Weapons
{
    public static class WeaponFactory
    {
        // Creates a weapon of the given type and level.
        public static Weapon CreateWeapon(WeaponTypes type, int level)
        {
            return new Weapon(type, level);
        }

        // Creates a random weapon.
        public static Weapon CreateRandomWeapon()
        {
            Random random = new Random();
         
            // Get random weapon type.
            Array types = Enum.GetValues(typeof(WeaponTypes));
            WeaponTypes randomType = (WeaponTypes)types.GetValue(random.Next(types.Length));

            // Get random level.
            int randomLevel = random.Next(100);

            return new Weapon(randomType, randomLevel);
        }
    }
}
