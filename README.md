# RPG Characters

This is my solution to the second assignment for the back-end part of Noroff .NET upskill course winter 2021.

This application does not contain any interaction other than showcasing creation of some characters and equipment.
First the characters and equipment from the examples in the assignment text are showcased.
Following this is _The Adventure of Bob the Warrior_, which tells the story of Bobs first day on earth.
Finally a random character is created.
This character has a random level between 1 and 100,
and has equipment picked at random from all possible types and materials,
and has a random level from the same level range as the character.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Features](#features)
- [Author](#author)


## Prerequisites
- .NET Framework
- Visual Studio 2017/19 OR Visual Studio Code

## Setup
- Clone the repository
- Open it in Visual Studio
- Run tests (optional)
- Run the app

## Features
- Heroes:
    - Have three possible classes: Warrior, Ranger and Mage.
    - Each class has a different set of starting stats.
    - Stats are Health, Strength, Dexterity and Intelligence.
    - Can level up by gaining experience.
    - Leveling up increases base stats differently for each class.
    - Can wield weapons and wear armor.
    - Can be randomly generated.

- Weapons:
    - Have three possible types: Melee, Ranged and Magic.
    - Weilding a weapon allowes heros to attack.
    - Each type uses a different stat to determine total damage.
    - Can be randomly generated.
 
- Armor:
    - Have three possible materials: Plate, Lether and Cloth.
    - Have three possible slots: Head, Body, Legs.
    - Wearing armor increases the heros stats.
    - Can be randomly generated.

## Author
[Birger Topphol](https://github.com/Birger89)
